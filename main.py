import logging
from flask import Flask
from markupsafe import escape
import json
import re


app = Flask(__name__)
"""Structurer le flux du fichier en dictionnaire"""
def users_dict(file_path):
    try:
        # Déclarer un tableau de type dictionnaire
        USERS_LIST = {}
        with open(file_path, "r") as data_file:
            i = -1
            for l in data_file:
                i = i+1
                USERS_LIST[i]={}
                ligne = l[:-1]
                ligne = ligne.split(",")
                USERS_LIST[i]['user'] =  ligne[0]
                USERS_LIST[i]['details'] =  {ligne[1]: ligne[2]+ligne[3], ligne[4]: ligne[5], ligne[6]: ligne[7], ligne[8]: ligne[9], ligne[10]: ligne[11], ligne[12]: ligne[13], ligne[14]: ligne[15]}
        return USERS_LIST
    except:
        return { "errors": {"msg": "File parse error}"}}

"""Afficher tout le distionnaire"""
def get_all_infos(my_dict):
    # Retourner le resultat au format JSON
    json_object = json.dumps(my_dict, indent = 5)
    return (json_object)


"""infos d'un utilisateur particulier"""
def get_user_infos(u, my_dict):
    INFO = {}
    for key,value in my_dict.items():
        if not value['user'] == str(u):
            continue
        INFO["Dernier changement de mot de passe"] = value['details']['Dernier changement de mot de passe']
        INFO["Fin de validité du mot de passe"] = value['details']["Fin de validité du mot de passe"]
        INFO["Mot de passe désactivé"] = value['details']["Mot de passe désactivé"]
        INFO["Fin de validité du compte"] = value['details']["Fin de validité du compte"]
        INFO["Nombre minimum de jours entre les changements de mot de passe"] = value['details']["Nombre minimum de jours entre les changements de mot de passe"]
        INFO["Nombre maximum de jours entre les changements de mot de passe"] = value['details']["Nombre maximum de jours entre les changements de mot de passe"]
        INFO["Nombre de jours davertissement avant la fin de validité du mot de passe"] = value['details']["Nombre de jours davertissement avant la fin de validité du mot de passe"]
    # Retourner le resultat au format JSON
    json_object = json.dumps(INFO, indent = 4)
    return str(json_object)

"""Les utilisateurs dont le nombre de jours d'arvertissement de fin de validité est > cnt """
def user_passwd_exp(cnt, my_dict):
    INFO = {}
    for key,value in my_dict.items():
        n = value['details']["Nombre de jours davertissement avant la fin de validité du mot de passe"]
        if not (int(n) > int(cnt)):
            continue
        INFO[value['user']] = {"Fin de validité du mot de passe": value['details']["Fin de validité du mot de passe"], "Nombre de jours davertissement avant la fin de validité du mot de passe": value['details']["Nombre de jours davertissement avant la fin de validité du mot de passe"]}
    # Retourner le resultat au format JSON
    json_object = json.dumps(INFO, indent = 4)
    return str(json_object)


""" APIs """
@app.route('/')
def api_default():
    MY_FILE = "list_users.json"
    MYDICT = users_dict(MY_FILE)
    rslt = get_all_infos(MYDICT)
    return F"{rslt}", 200

@app.route('/infos')
def api_all_infos():
    MY_FILE = "list_users.json"
    MYDICT = users_dict(MY_FILE)
    rslt = get_all_infos(MYDICT)
    return F"{rslt}", 200

@app.route('/infos/<username>')
def api_user_infos(username):
    root_pattern = re.compile('root')
    if root_pattern.match(username):
        msg = {}
        msg['Error'] = {"message": "This login name is not allowed"}
        return json.dumps(msg), 401
    else:
        MY_FILE = "list_users.json"
        MYDICT = users_dict(MY_FILE)
        rslt = get_user_infos(username, MYDICT)
        return F"{rslt}", 200

@app.route('/infos/pass/<nbre>')
def api_mdp_expire(nbre):
    MY_FILE = "list_users.json"
    MYDICT = users_dict(MY_FILE)
    int_nbre = int(nbre)
    rslt = user_passwd_exp(int_nbre, MYDICT)
    return F"{rslt}", 200


if __name__ == '__main__':
   app.run(debug = True)
