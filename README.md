# TP: Écriture des APIs avec Python et Flask
**Étudiant**: OKE Ulrich Enangnon

**Url du dépôt**: https://gitlab.imt-atlantique.fr/u22oke/pyflask.git

## Objectif
Les activités de ce projet entrent dans le cadre de la mise en application des connaissances de développement des API avec le framework Flask.

## Info:
Ce projet est un API REST très simple qui permet de recupérer des informations sur une liste de comptes d'utilisateur ajoutée à ce dépôt.

Nous nous sommes servi de l'outil en ligne de commande Linux  ```xidel``` pour générer le fichier brut ```list_users.json```.

Il n'est pas bien formaté. Il eut fallu la modifier manuellement pour qu'il soit accepté par l'application Flask.
## Developpement
- [**Etape 1**]: préparer l'environnement

```
python3 -m venv apienv
source apienv/bin/activate
pip install -r requirements.txt
```

- [**Etape 2**]: Copier le code et exécuter l'application
```
git clone https://gitlab.imt-atlantique.fr/u22oke/pyflask.git
cd myapp
export FLASK_APP=main.py
flask run 
```

## Test des API 
installer jq pour mieux présenter les résultats JSON renvoyés à l'issue des requêtes.

1. Requête /infos

Cette API permet de recupérer l'ensemble des informations de comptes d'utilisateurs

```
curl -S 127.0.0.1:5000/infos | jq "."

{
  "0": {
    "user": "root",
    "details": {
      "Dernier changement de mot de passe": " juin 29 2020",
      "Fin de validité du mot de passe": "jamais",
      "Mot de passe désactivé": " jamais",
      "Fin de validité du compte": "jamais",
      "Nombre minimum de jours entre les changements de mot de passe": "0",
      "Nombre maximum de jours entre les changements de mot de passe": "99999",
      "Nombre de jours davertissement avant la fin de validité du mot de passe": "7"
    }
  },
  "1": {
    "user": "tss",
    "details": {
      "Dernier changement de mot de passe": " avril 23 2020",
      "Fin de validité du mot de passe": "jamais",
      "Mot de passe désactivé": " jamais",
      "Fin de validité du compte": "jamais",
      "Nombre minimum de jours entre les changements de mot de passe": "0",
      "Nombre maximum de jours entre les changements de mot de passe": "99999",
      "Nombre de jours davertissement avant la fin de validité du mot de passe": "9"
    }
  },
  "2": {
    "user": "speech-dispatcher",
    "details": {
      "Dernier changement de mot de passe": "avril 23 2020",
      "Fin de validité du mot de passe": " jamais",
      "Mot de passe désactivé": "jamais",
      "Fin de validité du compte": "jamais",
      "Nombre minimum de jours entre les changements de mot de passe": "0",
      "Nombre maximum de jours entre les changements de mot de passe": "99999",
      "Nombre de jours davertissement avant la fin de validité du mot de passe": "7"
    }
  },
  ...
}
```

2. Requête /infos/username

Cette API permet de récupérer les informations d'un compte particulier excepté root. 

```
curl -S 127.0.0.1:5000/infos/root | jq "."

{
  "Error": {
    "message": "This login name is not allowed"
  }
}
```

3. Requête /infos/pass/nbre

Cette API permet de lister les utilisateurs dont les mots de passe ont un nombre de jours d'alerte de fin de validité supérieur a nbre.

```
curl -S 127.0.0.1:5000/infos/pass/7 | jq "."

{
  "tss": {
    "Fin de validité du mot de passe": "jamais",
    "Nombre de jours davertissement avant la fin de validité du mot de passe": "9"
  },
  "hplip": {
    "Fin de validité du mot de passe": "jamais",
    "Nombre de jours davertissement avant la fin de validité du mot de passe": "12"
  }
}
```

## Liens utiles 
### Regex

https://www.geeksforgeeks.org/pattern-matching-python-regex/

### HTTP return codes

https://developer.mozilla.org/en-US/docs/Web/HTTP/Status#client_error_responses

